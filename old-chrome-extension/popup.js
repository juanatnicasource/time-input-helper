import {HarvestWrapper} from "./HarvestWrapper.js";
import {init as tasksInit} from "./tasksTab.js"
import {init as githubInit} from "./githubTab.js"
import {init as configInit} from "./configTab.js"

//init all materialize plugins
//M.AutoInit();

M.Tabs.init(document.querySelector('.tabs'), {});

const harvest = new HarvestWrapper();
let assignmentsPromise = harvest.getProjectAssignments();

configInit(M);
tasksInit(harvest, assignmentsPromise, M);
githubInit(harvest, assignmentsPromise, M);
