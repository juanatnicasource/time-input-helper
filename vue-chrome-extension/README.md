This project was created with `kocal/vue-web-extension`, like: `vue init kocal/vue-web-extension web-extension`

For development:
1. `yarn`
2. `yarn run watch:dev`

The previous command will create a folder `dist/` this the one you need to point in chrome to load the extension

1. Open Chrome and enter chrome://extensions/ in the address bar. You should see a page displaying the extensions you’ve installed.

2. Activate Developer mode using the toggle in the top right-hand corner of the page. 

3. This should add an extra menu bar with the option Load unpacked. Click this button and select the `path/to/dist` folder created previously.