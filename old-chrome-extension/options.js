let submit = document.getElementById('submit');
submit.onclick = function (element) {

  chrome.storage.sync.set({accountId: accountId.value}, function () {
    console.log("Saved.");
  });
  chrome.storage.sync.set({accessToken: accessToken.value}, function () {
    console.log("Saved.");
  });
  chrome.storage.sync.set({email: email.value}, function () {
    console.log("Saved.");
  });
  chrome.storage.sync.set({userId: userId.value}, function () {
    console.log("Saved.");
  });
}

(function loadData() {
  chrome.storage.sync.get('accountId', function (data) {
    accountId.setAttribute('value', data.accountId);
  });
  chrome.storage.sync.get('accessToken', function (data) {
    accessToken.setAttribute('value', data.accessToken);
  });
  chrome.storage.sync.get('email', function (data) {
    email.setAttribute('value', data.email);
  });
  chrome.storage.sync.get('userId', function (data) {
    if (!data) {

    }
    userId.setAttribute('value', data.userId);
  });
})();