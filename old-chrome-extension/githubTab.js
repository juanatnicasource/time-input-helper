import {nextDay, getMonday} from "./helpers.js";
import {GithubWrapper} from "./GithubWrapper.js";

export function init(harvest, assignments, M) {

    /**
     * Modified version to allow text as option
     */
    M.Autocomplete.prototype.selectOption = function(el) {
        let text = typeof el === 'string' ? el: el.text().trim();
        this.el.value = text;
        this.$el.trigger('change');
        this._resetAutocomplete();
        this.close();

        // Handle onAutocomplete callback.
        if (typeof this.options.onAutocomplete === 'function') {
            this.options.onAutocomplete.call(this, text);
        }
    };

    let githubHarvestResult = document.getElementById('githubHarvestResult');
    let githubRetrieve = document.getElementById('githubRetrieve');
    let githubSave = document.getElementById('githubSave');
    let reloadPage = document.getElementById('reloadPage');
    let githubResults = document.getElementById('githubResults');
    let prevWeek = document.getElementById('prevWeek');
    let nextWeek = document.getElementById('nextWeek');
    let thisMonday = getMonday();

    let monday = getMonday().setHours(0 , 0, 0, 0);

    let projectsData = null;
    let taskId = M.Autocomplete.init(document.getElementById('harvestTaskId'), {});
    let harvestProject = M.Autocomplete.init(document.getElementById('harvestProjectId'), {
        onAutocomplete: () => {
            let selectedProject = projectsData.project_assignments.find(item => item.project.name === harvestProject.el.value);
            let pairObject = {};
            selectedProject.task_assignments.map(item => {
                pairObject[item.task.name] = null;
            });
            taskId.updateData(pairObject);
            taskId.el.value = '';
        }
    });
    let githubData = null;

    document.querySelector('#current-date').textContent =
        `${thisMonday.toLocaleDateString()} - ${nextDay(thisMonday, 4).toLocaleDateString()}`;

    assignments.then(data => {
        projectsData = data;
        let pairObject = {};
        data.project_assignments.map(item => {
            pairObject[item.project.name] = null;
        });
        harvestProject.updateData(pairObject);

        chrome.storage.sync.get(['repo', 'harvestProjectId', 'harvestTaskId'], data => {
            repo.setAttribute('value', data.repo || '');
            let savedProjectId = !!data.harvestProjectId ? parseInt(data.harvestProjectId) : null;
            if (!!savedProjectId) {
                let savedProject = projectsData.project_assignments.find(item => item.project.id === savedProjectId);
                harvestProject.selectOption(savedProject.project.name);
                let savedTaskId = !!data.harvestTaskId ? parseInt(data.harvestTaskId) : null;
                let savedTask = savedProject.task_assignments.find(item => item.task.id === savedTaskId);
                taskId.selectOption(savedTask.task.name);
            }
        });
    })

    githubRetrieve.onclick = function() {
        chrome.storage.sync.set({repo: repo.value}, function() {
            console.log("Saved.");
        });
        let selectedProject = projectsData.project_assignments.find(item => item.project.name === harvestProject.el.value);
        let selectedTask = selectedProject.task_assignments.find(item => item.task.name === taskId.el.value);
        chrome.storage.sync.set({harvestTaskId: selectedTask.task.id}, function() {
            console.log("Saved.");
        });
        chrome.storage.sync.set({harvestProjectId: selectedProject.project.id}, function() {
            console.log("Saved.");
        });
        githubAccess();
    }

    function githubAccess() {
        let days = [];
        document.querySelectorAll('#github .days input:checked').forEach(item => days.push(parseInt(item.value)));
        const githubWrapper = new GithubWrapper();
        githubWrapper.get(repo.value.trim(), nextDay(monday, 0), days)
            .then(jsonData => {
                githubData = jsonData;
                let results = '';
                if (!!Object.keys(jsonData).length) {
                    for (var key in jsonData) {
                        if (jsonData.hasOwnProperty(key)) {
                            results += `<div><h6 class="center-align">${key}</h6></div>`;
                            results += `<div class="row">
                                <div class="col s8"><textarea class="materialize-textarea" name="content${key}" id="content${key}" rows="3">${jsonData[key]}</textarea></div>
                                <div class="col s4"><input type="number" name="hours${key}" id="hours${key}" value="9"></div>
                            </div>`;
                            //console.log(key + " -> " + jsonData[key]);
                        }
                    }
                    githubSave.disabled = false;
                    reloadPage.disabled = false;
                } else {
                    githubSave.disabled = true;
                    reloadPage.disabled = true;
                    results += '<p class="flow-text">No commits found</p>'
                }

                githubResults.innerHTML = results;
            })
            .catch(message => {
                console.log(message);
                githubSave.disabled = true;
                reloadPage.disabled = true;
                githubResults.innerHTML = '<p class="flow-text">No commits found</p>';
            })

    }

    githubSave.onclick = function() {

        harvest.isReady().then(ready => {
            if (!ready) {
                githubHarvestResult.textContent = 'Config values not ready.';
                return;
            }
            let selectedProject = projectsData.project_assignments.find(item => item.project.name === harvestProject.el.value);
            let selectedTask = selectedProject.task_assignments.find(item => item.task.name === taskId.el.value);

            Promise.all(Object.keys(githubData).map((item, index) => {
                let payload = {};
                payload.spent_date = nextDay(thisMonday, index).toISOString().split('T')[0]; //rain demonstration
                //payload.spent_date = item;
                payload.hours = document.getElementById(`hours${item}`).value;
                payload.notes = document.getElementById(`content${item}`).value;
                payload.project_id = selectedProject.project.id;
                payload.task_id = selectedTask.task.id;
                return harvest.post(payload)
            }))
                .then(resp => {
                    console.log(resp);
                    githubHarvestResult.textContent = 'Entries saved into Harvest. Well done!';
                    if (!!reloadPage.checked) {
                        chrome.tabs.reload();
                    }
                })
        })

    }

    prevWeek.onclick = function() {
        monday = nextDay(monday, -7);
        updateLabel(monday);
    }

    nextWeek.onclick = function() {
        monday = nextDay(monday, 7);
        updateLabel(monday);
    }

    function updateLabel(date) {
        document.querySelector('#current-date').textContent =
            `${date.toLocaleDateString()} - ${nextDay(date, 4).toLocaleDateString()}`;
    }

}