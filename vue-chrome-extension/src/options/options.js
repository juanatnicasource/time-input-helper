import Vue from 'vue';
// import { MdButton, MdContent, MdTabs, MdField, MdInput } from 'vue-material/dist/components'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import App from './App';

Vue.use(VueMaterial)
/*Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)
Vue.use(MdField)
Vue.use(MdInput)*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
});
