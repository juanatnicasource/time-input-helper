export class HarvestWrapper {

    constructor() {
        this.ready = new Promise((resolve, reject) => {
            chrome.storage.sync.get(['accountId', 'accessToken', 'email', 'userId', 'company'],
                data => {
                    this.config = data;
                    this.baseUrl = 'https://api.harvestapp.com/v2/';
                    this.myHeaders = new Headers({
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${data.accessToken}`,
                        "Harvest-Account-Id": data.accountId,
                        "User-Agent": `Rain ${data.email}`,
                    });
                    this.userId = data.userId;
                    this.company = data.company;
                    resolve(true);
                });
        });
        this.rainLunchPayload = {
            project_id: 16071102,
            task_id: 7745134,
            hours: 0.5,
            notes: 'Lunch'
        };

        this.deependLunchPayload = {"project_id":18302328,"task_id":10401815,hours:"0.50","notes":"Lunch"};

        this.rainPtoPayload = {
            project_id: 16070692,
            task_id: 3164942,
            hours: 9.5,
            notes: notes.value
        };

        this.deependPtoPayload = {"project_id":18302328,"task_id":4435418,"hours":"9.5"}
    }

    post(body) {
        return this.ready.then(() => {
            body.user_id = this.userId;
            let myInit = { method: 'POST',
                headers: this.myHeaders,
                mode: 'cors',
                cache: 'default',
                body: JSON.stringify(body)
            };

            return fetch(new Request(`${this.baseUrl}time_entries`, myInit)).then(resp => resp.json());
        })

    }

    getProjectAssignments() {

        return this.ready.then(() => {
            if(!this.userId) {
                return Promise.reject('User id not found.')
            }

            let myInit = { method: 'GET',
                headers: this.myHeaders,
                mode: 'cors',
                cache: 'default',
            };

            return fetch(new Request(`${this.baseUrl}/users/${this.userId}/project_assignments`, myInit))
                .then(resp => resp.json());
        })
    }

    getLunchPayload() {

        if (this.company === 'rain')
            return this.rainLunchPayload;

        return this.deependLunchPayload;
    }

    getPtoPayload() {

        if (this.company === 'rain')
            return this.rainPtoPayload;

        return this.deependPtoPayload;
    }

    /**
     * Will verify if configuration is correct
     */
    isReady() {
        return this.ready.then(() => {
            return !!this.config && !!this.config.accessToken && !!this.config.accessToken && !!this.config.email && !!this.config.userId;
        })
    }

}