The main folder is `vue-chrome-extension`.

The code in `old-chrome-extension` is not maintained anymore, and is provided here only as a reference

Harvest API
https://help.getharvest.com/api-v2/

Access Token:
1. Log in to your harvest account
2. Go to https://id.getharvest.com/developers
3. Go to Personal Access Tokens section and Create a new Personal Access Token
4. Type a name for your token, and after the token is created copy and paste the "Your token" and "Account Id" values from Harvest to this form 

Gitlab API
https://docs.gitlab.com/ce/api/README.html

Access Token
1. Log in to GitLab.
2. In the upper-right corner, click your avatar and select Settings.
3. On the User Settings menu, select Access Tokens.
4. Choose a name and optional expiry date for the token.
5. Choose the desired scopes.
6. Click the Create personal access token button.
7. Save the personal access token somewhere safe. Once you leave or refresh the page, you won’t be able to access it again.

Github API

Access Token
1. Go to https://github.com/settings/tokens
2. At the top right corner, click on "Generate new token" button
3. Type a name for the token and under the Scope options select all the options within the "repo" group
