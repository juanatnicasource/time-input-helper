export function nextDay(d, diff) {
    d = new Date(d);
    return new Date(d.setDate(d.getDate() + diff));
}
export function getMonday(d = new Date()) {
    d = new Date(d);
    let day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}