import {nextDay} from "./helpers.js";

export class GithubWrapper {
    constructor() {
        this.ready = new Promise((resolve, reject) => {
            chrome.storage.sync.get(['githubAccessToken', 'githubAuthor'],
                data => {
                    console.log(data);
                    if (!Object.keys(data).length) {
                        console.error('Config not ready')
                    }
                    this.author = data.githubAuthor;
                    this.myHeaders = new Headers({
                        "Authorization": `token ${data.githubAccessToken}`
                    });

                    resolve(true);
                });
        });

    }

    get(repo, startDate, filter) {
        return this.ready.then(() => {

            let myInit = { method: 'GET', headers: this.myHeaders};

            return fetch(new Request(`https://api.github.com/repos/` +
                `${repo}/commits` +
                `?author=${this.author}` +
                `&since=${startDate.toISOString()}&until=${nextDay(startDate, 4).toISOString()}`, myInit))
                .then(resp => resp.json())
                .then(jsonData => {
                    if(jsonData.message === 'Not Found') {
                        return Promise.reject(jsonData.message)
                    }
                    return jsonData.reduce((filtered, item) => {
                        let day = new Date(item.commit.committer.date);
                        if (item.commit.message.indexOf('Merge pull request') === -1 && filter.indexOf(day.getDay()) !== -1) {
                            if (filtered[day.toISOString().split('T')[0]]) {
                                filtered[day.toISOString().split('T')[0]] += item.commit.message + '\n';
                            } else {
                                filtered[day.toISOString().split('T')[0]] = item.commit.message + '\n';
                            }

                        }
                        return filtered;
                    }, {});
                })
        })
    }

}