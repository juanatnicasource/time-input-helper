const BASE_API = `https://gitlab.com/api/v4/`;
export class GitlabAPI {

    constructor() {
        this.ready = new Promise((resolve, reject) => {
            chrome.storage.sync.get(['gitlabAccessToken', 'email'],
                data => {
                    console.log(data);
                    if (!Object.keys(data).length) {
                        console.error('Config not ready')
                    }
                    this.email = data.email;
                    this.myHeaders = new Headers({
                        "Authorization": `Bearer ${data.gitlabAccessToken}`
                    });

                    resolve(true);
                });
        });

    }

    async projects(search) {
        return this.ready.then(() => {

            let myInit = { method: 'GET', headers: this.myHeaders};

            return fetch(new Request(`${BASE_API}projects?simple=true&search=${search}`, myInit))
              .then(resp => resp.json())
              .then(jsonData => {
                if (jsonData.message === 'Not Found') {
                  return Promise.reject(jsonData.message)
                }
                console.log(jsonData);
                return jsonData;
              });
        })
    }

    async commits(project, since, until, email = null) {
        return this.ready.then(() => {

            let myInit = { method: 'GET', headers: this.myHeaders};

            return fetch(new Request(`${BASE_API}projects/${project}/repository/commits?since=${since}&until=${until}`, myInit))
              .then(resp => resp.json())
              .then(jsonData => {
                if (jsonData.message === 'Not Found') {
                  return Promise.reject(jsonData.message)
                }
                return jsonData.filter(item => item.committer_email === this.email || item.committer_email === email)
                  .filter(item => item.message.indexOf('Merge') === -1);
              });
        })
    }

}