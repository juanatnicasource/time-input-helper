export function init(M) {
    let configHarvestSave = document.getElementById('configHarvestSave');
    let configGithubSave = document.getElementById('configGithubSave');
    let getUserIdButton = document.getElementById('getUserId');
    let accountId = document.getElementById('accountId');
    let accessToken = document.getElementById('accessToken');
    let email = document.getElementById('email');
    let userId = document.getElementById('userId');
    let githubAuthor = document.getElementById('githubAuthor');
    let githubAccessToken = document.getElementById('githubAccessToken');
    let collapsibles = document.querySelectorAll('.custom-collapsible');

    function getUserId() {
        return document.querySelector("#popover-user > a:nth-child(2)").href
    }

    chrome.storage.sync.get(['accountId', 'accessToken', 'email', 'userId', 'company'], data => {
        accountId.setAttribute('value', data.accountId || '');
        accessToken.setAttribute('value', data.accessToken || '');
        email.setAttribute('value', data.email || '');
        document.getElementById(`${data.company}Option`).checked = true;

        if (!!data.userId) {
            userId.setAttribute('value', data.userId);
        } else {
            //let's try to automatically recover the User Id

            chrome.tabs.executeScript({
                code: '(' + getUserId + ')();' //argument here is a string but function.toString() returns function's code
            }, (result) => {
                const regex = /(?<=\/)\d.*(?=\/)/gm;
                const str = result[0];
                let matches;

                while ((matches = regex.exec(str)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (matches.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }

                    // The result can be accessed through the `m`-variable.
                    matches.forEach((match, groupIndex) => {
                        console.log(`Found match, group ${groupIndex}: ${match}`);
                        userId.setAttribute('value', match);
                    });
                }

                chrome.storage.sync.set({userId: userId.value}, function() {
                    window.location = window.location;
                });

            });
        }
    });

    chrome.storage.sync.get(['githubAuthor', 'githubAccessToken'], data => {
        githubAuthor.setAttribute('value', data.githubAuthor || '');
        githubAccessToken.setAttribute('value', data.githubAccessToken || '');
    });

    document.addEventListener('DOMContentLoaded', function() {
        let elems = document.querySelectorAll('.collapsible');
        let instances = M.Collapsible.init(elems, {accordion: false});
    });

    configHarvestSave.onclick = function () {
        chrome.storage.sync.set({accountId: accountId.value}, function() {
            console.log("Saved.");
        });
        chrome.storage.sync.set({accessToken: accessToken.value}, function() {
            console.log("Saved.");
        });
        chrome.storage.sync.set({email: email.value}, function() {
            console.log("Saved.");
        });
        chrome.storage.sync.set({userId: userId.value}, function() {
            console.log("Saved.");
        });
        let companySelected = document.querySelector('input[name=company]:checked');
        chrome.storage.sync.set({company: companySelected.value}, function() {
            let divs = document.querySelectorAll('input[name=group1]');
            for (let i = 0; i < divs.length; ++i) {
                if (divs[i].value === "custom" && companySelected.value === 'other') {
                    divs[i].checked = true;
                }
                if (divs[i].value !== "custom" && companySelected.value === 'other') {
                    divs[i].setAttribute('disabled', 'disabled');
                } else {
                    divs[i].removeAttribute('disabled');
                }
            }
        });
    }

    configGithubSave.onclick = function () {
        chrome.storage.sync.set({githubAuthor: githubAuthor.value}, function() {
            console.log("Saved.");
        });
        chrome.storage.sync.set({githubAccessToken: githubAccessToken.value}, function() {
            console.log("Saved.");
        });
    }

    collapsibles.forEach(el => el.addEventListener('click', (event) => {
        event.preventDefault();
        event.stopPropagation();
        let target = event.currentTarget.getAttribute('data-target');
        let targetEl = document.querySelector(target);
        targetEl.hidden = !targetEl.hidden;
    }));

    getUserIdButton.onclick = function () {
        scrapUserId();
    }

    function scrapUserId() {
        chrome.tabs.executeScript({
            code: '(' + getUserId + ')();' //argument here is a string but function.toString() returns function's code
        }, (result) => {
            const regex = /(?<=\/)\d.*(?=\/)/gm;
            const str = result[0];
            let matches;

            while ((matches = regex.exec(str)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (matches.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                // The result can be accessed through the `m`-variable.
                matches.forEach((match, groupIndex) => {
                    console.log(`Found match, group ${groupIndex}: ${match}`);
                    userId.setAttribute('value', match);
                });
            }

        });
    }
}