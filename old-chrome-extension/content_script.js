function github() {
  let monday = getMonday().setHours(0 , 0, 0, 0);
  let githubAccessToken = '7bc940fc810a9dce69530639aee8682526d781c2';
  let author = 'jgutix';
  let myHeaders = new Headers({
    "Authorization": `token ${githubAccessToken}`
  });

  let myInit = { method: 'GET', headers: myHeaders};

  let myRequest = new Request(`https://api.github.com/repos/mediarain/harmonsgrocery/commits?author=${author}`, myInit);
  fetch(myRequest)
    .then(response => response.json())
    .then(jsonData => {
      /*jsonData = jsonData.filter(item => {
        return item.commit.message.indexOf('Merge pull request') === -1 && (new Date(item.commit.committer.date)) > monday
      })
      console.log(jsonData);*/
      return jsonData.reduce((filtered, item) => {
        let day = new Date(item.commit.committer.date);
        if (item.commit.message.indexOf('Merge pull request') === -1 && day > monday) {
          if (filtered[day.toISOString().split('T')[0]]) {
            filtered[day.toISOString().split('T')[0]] += item.commit.message + '\n';
          } else {
            filtered[day.toISOString().split('T')[0]] = item.commit.message + '\n';
          }

        }
        return filtered;
      }, {});

    })
    .then(week => {
      console.log(week);
    });

}

github();

function getMonday(d = new Date()) {
  d = new Date(d);
  var day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}