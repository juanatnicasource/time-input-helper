import {nextDay, getMonday} from "./helpers.js";

export function init(harvest, assignments, M) {
    let tasksResult = document.getElementById('tasksResult');
    let reloadPageTasks = document.getElementById('reloadPageTasks');
    let taskTime = document.getElementById('taskTime');
    let notes = document.getElementById('notes');

    let projectsData = null;
    let taskId = M.Autocomplete.init(document.getElementById('taskId'), {});
    let projectId = M.Autocomplete.init(document.getElementById('projectId'), {
        onAutocomplete: () => {
            let selectedProject = projectsData.project_assignments.find(item => item.project.name === projectId.el.value);
            let pairObject = {};
            selectedProject.task_assignments.map(item => {
                pairObject[item.task.name] = null;
            });
            taskId.updateData(pairObject);
            taskId.el.value = '';
        }
    });
    let submitTasks = document.getElementById('submit');
    let thisMonday = getMonday();

    let lastMonday = nextDay(thisMonday, -7);

    document.querySelector('#thisWeekLabel span').textContent =
        `This Week (${thisMonday.toLocaleDateString()} - ${nextDay(thisMonday, 4).toLocaleDateString()})`;

    document.querySelector('#lastWeekLabel span').textContent =
        `Last Week (${lastMonday.toLocaleDateString()} - ${nextDay(lastMonday, 4).toLocaleDateString()})`;

    const els = document.querySelectorAll('[name="group1"]');

    const optionsOnChange = (e) => {
        let selected = document.querySelector('input[name=group1]:checked').value;
        switch (selected) {
            case 'lunch':
                projectId.el.disabled = true;
                taskId.el.disabled = true;
                projectId.el.value = '';
                taskId.el.value = '';
                notes.value = 'Lunch';
                notes.disabled = true;
                taskTime.value = 0.5;
                break;
            case 'pto':
                projectId.el.disabled = true;
                taskId.el.disabled = true;
                projectId.el.value = '';
                taskId.el.value = '';
                notes.value = 'PTO';
                taskTime.value = 9.5;
                break;
            case 'custom':
                projectId.el.disabled = false;
                taskId.el.disabled = false;
                notes.disabled = false;
                notes.value = '';
                break;
        }

    }

    els.forEach(el => el.addEventListener('change', optionsOnChange));

    chrome.storage.sync.get(['company'], data => {
        if (data.company !== 'other') {
            let divs = document.querySelectorAll('input[name=group1]');
            for (let i = 0; i < divs.length; ++i) {
                divs[i].removeAttribute('disabled');
            }
        }
    })
    assignments.then(data => {
        projectsData = data;
        let pairObject = {};
        data.project_assignments.map(item => {
            pairObject[item.project.name] = null;
        });
        projectId.updateData(pairObject);
    }).catch(exception => {
        //tasksResult.textContent = 'Config values not ready.';
        M.toast({html: 'Config values not ready'})
    })

    submitTasks.onclick = function (element) {

        // let harvest = new HarvestWrapper();
        harvest.isReady().then(ready => {
            if (!ready) {
                tasksResult.textContent = 'Config values not ready.';
                return;
            }
            let initPayload = null;
            let selected = document.querySelector('input[name=group1]:checked').value;
            switch (selected) {
                case 'lunch':
                    initPayload = harvest.getLunchPayload();
                    break;
                case 'pto':
                    initPayload = harvest.getPtoPayload();
                    initPayload.notes = notes.value;
                    break;
                case 'custom':
                    let selectedProject = projectsData.project_assignments.find(item => item.project.name === projectId.el.value);
                    let selectedTask = selectedProject.task_assignments.find(item => item.task.name === taskId.el.value);
                    initPayload = {
                        project_id: selectedProject.project.id,
                        task_id: selectedTask.task.id,
                        hours: taskTime.value,
                        notes: notes.value
                    };
                    break;
            }

            Promise.all([...Array(5).keys()].map(item => {
                let el = document.getElementById(`day${item}`);
                if (!!el && el.checked) {
                    let day = nextDay(thisMonday, item);
                    let payload = {...initPayload};
                    payload.spent_date = day.toISOString().split('T')[0];
                    return harvest.post(payload)
                }
                return null;
            }))
                .then(resp => {
                    console.log(resp);
                    tasksResult.textContent = 'Entries saved into Harvest. Well done!';
                    if (!!reloadPageTasks.checked) {
                        chrome.tabs.reload();
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        })

    };
}